package main

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

// Content represents the structure of online content
type Content struct {
	ID             string    `json:"id"`
	Title          string    `json:"title"`
	Description    string    `json:"description"`
	CreationDate   time.Time `json:"creationDate"`
	ContentCreator string    `json:"contentCreator"`
	Distributor    string    `json:"distributor"`
	ContentStatus  bool      `json:"contentStatus"`
}

// ContentContract provides functions for managing content
type ContentContract struct {
	contractapi.Contract
}

// CreateContent adds a new content to the world state
func (c *ContentContract) CreateContent(ctx contractapi.TransactionContextInterface, id string, title string,
	description string, creationDate time.Time, contentCreator string, distributor string, contentStatus bool) error {
	content := Content{
		ID:             id,
		Title:          title,
		Description:    description,
		CreationDate:   creationDate,
		ContentCreator: contentCreator,
		Distributor:    distributor,
		ContentStatus:  contentStatus,
	}
	contentJSON, err := json.Marshal(content)
	if err != nil {
		return err
	}
	err = ctx.GetStub().PutState(id, contentJSON)
	if err != nil {
		return fmt.Errorf("failed to put to world state. %v", err)
	}
	eventPayload := fmt.Sprintf("Created content: %s", id)
	err = ctx.GetStub().SetEvent("CreateContent", []byte(eventPayload))
	if err != nil {
		return fmt.Errorf("event failed to register. %v", err)
	}
	return nil
}

// ReadContent retrieves a content from the world state
func (c *ContentContract) ReadContent(ctx contractapi.TransactionContextInterface, id string) (*Content, error) {
	contentJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return nil, fmt.Errorf("failed to read from world state: %v", err)
	}
	if contentJSON == nil {
		return nil, fmt.Errorf("the content %s does not exist", id)
	}
	var content Content
	err = json.Unmarshal(contentJSON, &content)
	if err != nil {
		return nil, err
	}
	eventPayload := fmt.Sprintf("Read content: %s", id)
	err = ctx.GetStub().SetEvent("ReadContent", []byte(eventPayload))
	if err != nil {
		return nil, fmt.Errorf("event failed to register. %v", err)
	}
	return &content, nil
}

// UpdateContent updates an existing content in the world state
func (c *ContentContract) UpdateContent(ctx contractapi.TransactionContextInterface, id string, title string,
	description string, creationDate time.Time, contentCreator string, distributor string, contentStatus bool) error {
	content, err := c.ReadContent(ctx, id)
	if err != nil {
		return err
	}
	content.Title = title
	content.Description = description
	content.CreationDate = creationDate
	content.ContentCreator = contentCreator
	content.Distributor = distributor
	content.ContentStatus = contentStatus
	contentJSON, err := json.Marshal(content)
	if err != nil {
		return err
	}
	err = ctx.GetStub().PutState(id, contentJSON)
	if err != nil {
		return fmt.Errorf("failed to put to world state. %v", err)
	}
	eventPayload := fmt.Sprintf("Updated content: %s", id)
	err = ctx.GetStub().SetEvent("UpdateContent", []byte(eventPayload))
	if err != nil {
		return fmt.Errorf("event failed to register. %v", err)
	}
	return nil
}

// DeleteContent deletes a content from the world state
func (c *ContentContract) DeleteContent(ctx contractapi.TransactionContextInterface, id string) error {
	err := ctx.GetStub().DelState(id)
	if err != nil {
		return fmt.Errorf("failed to delete state: %v", err)
	}
	eventPayload := fmt.Sprintf("Deleted content: %s", id)
	err = ctx.GetStub().SetEvent("DeleteContent", []byte(eventPayload))
	if err != nil {
		return fmt.Errorf("event failed to register. %v", err)
	}
	return nil
}

func main() {
	chaincode, err := contractapi.NewChaincode(new(ContentContract))
	if err != nil {
		fmt.Printf("Error create content chaincode: %s", err.Error())
		return
	}
	if err := chaincode.Start(); err != nil {
		fmt.Printf("Error starting content chaincode: %s", err.Error())
	}
}
