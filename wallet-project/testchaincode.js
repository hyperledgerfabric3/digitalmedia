const { Gateway, Wallets } = require('fabric-network');
const fs = require('fs');
const path = require('path');

function prettyJSONString(inputString) {
    return JSON.stringify(JSON.parse(inputString), null, 2);
}

async function main() {
    // Load the network configuration
    const ccpPath = path.resolve(__dirname, '.', 'connection.json');
    const ccp = JSON.parse(fs.readFileSync(ccpPath, 'utf8'));

    // Create a new file system based wallet for managing identities.
    const walletPath = path.join(process.cwd(), 'wallet');
    const wallet = await Wallets.newFileSystemWallet(walletPath);

    // Check to see if we've already enrolled the user.
    const identity = await wallet.get('Admin@contentcreator.com');
    if (!identity) {
        console.log(`An identity for the user "Admin@contentcreator.com" does not exist in the wallet`);
        return;
    }

    // Create a new gateway instance for interacting with the fabric network.
    const gateway = new Gateway();
    try {
        // Connect to the gateway using the identity from wallet and the connection profile.
        await gateway.connect(ccp, { wallet, identity: identity, discovery: { enabled: false, asLocalhost: false } });

        // Now connected to the gateway.
        console.log('Connected to the gateway.');

        // Get the network (channel) and contract.
        const network = await gateway.getNetwork('digitalrightschannel');
        const contract = network.getContract('digitalmediamgt');

        // CREATE IP ASSET
        console.log('\n--> Submit Transaction: CreateContent, creates new IP asset');
        await contract.submitTransaction('CreateContent', '1', 'trip to paro', 'it is a vlog', '2024-01-01', 'drukden', 'jigme', 'false');
        console.log('*** Result: committed');

        

     
    } finally {
        // Disconnect from the gateway when you're done.
        gateway.disconnect();
    }
}

main().catch(console.error);
