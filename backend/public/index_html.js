$(document).ready(function () {
    // Function to handle form submission for creating intellectual property
    $('#createIPForm').on('submit', function (e) {
        e.preventDefault(); // Prevent the default form submission behavior

        // Extract data from the form
        var contentID = $('#contentID').val();
        var contentTitle = $('#contentTitle').val();
        var contentDescription = $('#contentDescription').val();
        var creationDate = $('#creationDate').val();
        var contentCreator = $('#contentCreator').val();
        var distributor = $('#distributor').val();
        var contentStatus = $('#editCreationStatus').val();



        // Create data object to send to the server
        var data = {
            id: contentID,
            title: contentTitle,
            description:contentDescription,
            creationDate: creationDate,
            contentCreator: contentCreator,
            distributor: distributor,
            contentStatus: contentStatus
        };

        // Make an AJAX POST request to create intellectual property
        $.ajax({
            url: '/content/', // Replace with the appropriate URL for your backend
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (result) {
                // Show success message modal
                var myModal = new bootstrap.Modal($('#successModal'), {});
                myModal.show();

                // Reset the form
                $('form')[0].reset();
            },
            error: function (xhr, status, error) {
                // Handle error (e.g., display an error message)
                console.log(xhr.responseText);
                $('#ipMessage').text("Error: " + xhr.responseText);
            }
        });
    });

    // Function to handle form submission for retrieving intellectual property information
    $('#getIPForm').on('submit', function (e) {
        e.preventDefault(); // Prevent the default form submission behavior

        // Extract the IP ID entered by the user
        var searchId = $('#searchId').val();

        // Make an AJAX GET request to retrieve intellectual property information
        $.ajax({
            url: '/content/' + searchId, // Replace with the appropriate URL for your backend
            type: 'GET',
            success: function (data) {
                data = JSON.parse(data);
                // Update the edit form fields with the retrieved intellectual property information
                $('#editContentID').val(data.id);
                $('#editcontentTitle').val(data.title);
                $('#editContentdescription').val(data.description);
                $('#editCreationDate').val(data.creationDate);
                $('#editcontentCreator').val(data.contentCreator);
                $('#editDistributor').val(data.distributor);
                $('#editContentStatus').val(data.contentStatus);

                // Show the edit form
                $('#editIPForm').removeClass('d-none');
            },
            error: function (xhr, status, error) {
                // Handle error (e.g., display an error message)
                console.log(xhr.responseText);
                $('#ipMessage').text("Error: " + xhr.responseText);
            }
        });
    });

    // Function to handle the deletion of intellectual property
    $('#deleteContentButton').click(function () {
        var ipId = $('#editContentID').text(); // Get the IP ID

        // Make an AJAX DELETE request to delete intellectual property
        $.ajax({
            url: 'content/' + ipId, // Append the IP ID to your API endpoint
            type: 'DELETE',
            success: function (result) {
                // Show success message modal
                var myModal = new bootstrap.Modal($('#successModal'), {});
                myModal.show();

                // Clear the edit form
                clearCard();
            },
            error: function (xhr, status, error) {
                // Handle error (e.g., display an error message)
                console.log(xhr.responseText, status);
                $('#ipMessage').text("Error: " + xhr.responseText);
            }
        });
    });
    // Function to handle the update of intellectual property
    $('#updatContentButton').click(function () {
        // Extract data from the edit form
        var contentId = $('#editContentID').val();
        var contentTitle = $('#editcontentTitle').val();
        var contentDescription = $('#editContentdescription').val();
        var CreationDate = $('#editCreationDate').val();
        var contentCreator = $('#editcontentCreator').val();
        var Distributor = $('#editDistributor').val();
        var contentStatus = $('#editContentStatus').val();


        // Log extracted values for debugging
        console.log("Updating IP with ID:", contentId);
        console.log("New title:", contentTitle);
        console.log("New description:", contentDescription);
        console.log("New date:", CreationDate);
        console.log("New creator:", contentCreator);
        console.log("New distributor:", Distributor);
        console.log("New status:", contentStatus);


        // Create data object to send to the server
        var data = {
            contentId: contentId,
            contentTitle:contentTitle,
            contentDescription:contentDescription,
            CreationDate:CreationDate,
            contentCreator:contentCreator,
            Distributor:Distributor,
            contentStatus:contentStatus
        };

        // Make an AJAX PUT request to update intellectual property
        $.ajax({
            url: '/content/' + ipId, // Append the IP ID to your API endpoint
            type: 'PUT',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (result) {
                console.log("Update successful:", result);
                // Show success message modal
                var myModal = new bootstrap.Modal($('#successModal'), {});
                myModal.show();

                // Clear the edit form
                clearCard();
            },
            error: function (xhr, status, error) {
                // Handle error (e.g., display an error message)
                console.log("Error status:", status);
                console.log("Error response:", xhr.responseText);
                $('#ipMessage').text("Error: " + xhr.responseText);
            }
        });
    });
});
