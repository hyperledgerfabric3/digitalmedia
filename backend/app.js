const express = require('express');
const { Gateway, Wallets } = require('fabric-network');
const fs = require('fs');
const path = require('path');

const app = express();
app.use(express.json());

// Static Middleware
app.use(express.static(path.join(__dirname, 'public')));

app.post('/content/', async (req, res) => {
    try {
        const { id, title, description, creationDate, contentCreator, distributor, contentStatus } = req.body;
        const result = await submitTransaction('CreateContent', id, title, description, creationDate, contentCreator, distributor, contentStatus);
        res.status(204).send(result);
    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
        res.status(500).send(`Failed to submit transaction: ${error}`);
    }
});

app.get('/content/:id', async (req, res) => {
    try {
        const { id } = req.params;
        const result = await evaluateTransaction('ReadContent', id);
        res.status(200).send(result);
    } catch (error) {
        console.error(`Failed to evaluate transaction: ${error}`);
        res.status(404).send(`Failed to evaluate transaction: ${error}`);
    }
});


app.put('/content/:id', async (req, res) => {
    try {
        const { id } = req.params;
        const { title, description, creationDate, contentCreator, distributor, contentStatus } = req.body;
        console.log(`Updating IP Asset with ID: ${id}, New title: ${title},newdescription:${description}, creationDate:${creationDate},new contentcreator:${contentCreator} new distributor:${distributor}, status: ${contentStatus}`); // Debugging info
        const result = await submitTransaction('UpdateContent', id, title, description, creationDate, contentCreator, distributor, contentStatus);
        console.log(`Transaction result: ${result}`); // Debugging info
        res.status(204).send(result);
    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
        res.status(500).send(`Failed to submit transaction: ${error}`);
    }
});
app.delete('/content/:id', async (req, res) => {
    try {
        const { id } = req.params;
        const result = await submitTransaction('DeleteContent', id);
        res.send(result);
    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
        res.status(500).send(`Failed to submit transaction: ${error}`);
    }
});



async function getContract() {
    const walletPath = path.join(process.cwd(), 'wallet');
    const wallet = await Wallets.newFileSystemWallet(walletPath);
    const identity = await wallet.get('Admin@contentcreator.com');
    const gateway = new Gateway();
    const connectionProfile = JSON.parse(fs.readFileSync(path.resolve(__dirname, 'connection.json'), 'utf8'));
    const connectionOptions = { wallet, identity: identity, discovery: { enabled: false, asLocalhost: true } };
    await gateway.connect(connectionProfile, connectionOptions);
    const network = await gateway.getNetwork('digitalrightschannel');
    const contract = network.getContract('digitalmediamgt');
    return contract;
}

async function submitTransaction(functionName, ...args) {
    const contract = await getContract();
    const result = await contract.submitTransaction(functionName, ...args);
    return result.toString();
}

async function evaluateTransaction(functionName, ...args) {
    const contract = await getContract();
    const result = await contract.evaluateTransaction(functionName, ...args);
    return result.toString();
}

app.get('/', (req, res) => {
    res.send('Hello, World!');
});

module.exports = app; // Exporting app
